﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Lab_3_Kyrylo
{
    abstract class Information
    {
        public int Money { get; set; }
        public string Name { get; set; }
        public Information(int money,string name)
        {
            this.Money = money;
            this.Name = name;
        }
        public abstract void Info();
    }
    class GES : Information
    {
        List<Client> ClientList = new List<Client>();
        List<Service> ServiceList = new List<Service>();
        public GES(string name, int money) : base(money, name)
        {
            this.Money = 0;
        }
        public void AddClient(string name)
        {
            ClientList.Add(new Client(name, 0));
        }
        public void AddService(string name, int money)
        {
            ServiceList.Add(new Service(name, money));
        }
        public void ClientInfo()
        {
            int bank = 0;
            foreach(Client cli in ClientList)
            {
                cli.Info();
                bank += cli.Money;
            }
            Console.WriteLine($"All bank: {bank}");
        }
        public void ServiceInfo()
        {
            foreach (Service cal in ServiceList)
            {
                cal.Info();
            }
        }
        public void BuyService()
        {
            try{
            ClientInfo();
            Console.WriteLine("Choose client: ");
            int clientindex = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" ");
            ServiceInfo();
            Console.WriteLine("Choose your Service: ");
            int Serviceindex = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" ");
            ClientList[clientindex - 1].AddService(ServiceList[Serviceindex - 1].Name,ServiceList[Serviceindex - 1].Money);
            }
            catch
            {
                Console.WriteLine("Sorry something went wrong");
            }
        }
        public override void Info()
        {
            ClientInfo();
            Console.WriteLine("Enter client name: ");
            string ServiceName = Console.ReadLine();
            foreach(Client cli in ClientList)
            {
                if(cli.Name == ServiceName)
                {
                    foreach(string i in cli.Services)
                    {
                        Console.WriteLine("Service name: " + i);
                    }
                }
            }
        }
    }
    class Client : Information
    {
        public static int ID { get; set; }
        public int id { get; set; }
        public List<string> Services = new List<string>();
        public Client(string name, int money) : base(money, name)
        {
            this.Money = 0;
            this.id = ID++;

        }
        public void AddService(string Servicename, int money)
        {
            Services.Add(Servicename);
            Money += money;
        }
        public override void Info()
        {
            Console.WriteLine(id+1 + " : " + "Name: " + Name + " | Money has been spend: " + Money);
        }
    }
    class Service : Information
    {
        public static int ID { get; set; }
        public int id { get; set; }
        public Service(string name, int money) : base(money, name)
        {
            this.id = ID++;
        }
        public override void Info()
        {
            Console.WriteLine(id+1 + " : "+"Name: " + Name + " | Cost: " + Money);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            bool end = true;
            GES Gek = new GES("Gek", 0);
            while(end)
            {
                Console.WriteLine("1.Add client"); // Ввести id клиента чтобы выбрать
                Console.WriteLine("2.Add Service"); // Ввести id сервиса чтобы выбрать
                Console.WriteLine("3.Client info"); // Покажет информацию о клиентах
                Console.WriteLine("4.Search by client name"); // Ввести имя(string) клиента чтобы выбрать
                Console.WriteLine("5.Order service"); //Ввести id клиента чтобы выбрать затем ввести id сервиса чтобы выбрать
                Console.WriteLine("6.Exit"); // Выйти из программы

                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        Console.WriteLine("Enter name: ");
                        string ClientName = Console.ReadLine();
                        Gek.AddClient(ClientName);
                        break;

                    case 2:
                        Console.WriteLine("Enter name: ");
                        string ServiceName = Console.ReadLine();
                        Console.WriteLine("Enter cost: ");
                        int ServiceCost = Convert.ToInt32(Console.ReadLine());
                        Gek.AddService(ServiceName, ServiceCost);
                        break;

                    case 3:
                        Gek.ClientInfo();
                        break;

                    case 4:
                        Gek.Info();
                        break;

                    case 5:
                        Gek.BuyService();
                        break;
                    case 6:
                        end = false;
                        break;
                }
            }
        }
    }
}